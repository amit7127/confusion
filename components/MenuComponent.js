import React, { Component } from "react";
import { View, FlatList, Text } from "react-native";
import { Tile } from "react-native-elements";
import { connect } from "react-redux";
import { baseUrl } from "../shared/baseUrl";
import { Loading } from "./LoadingComponent";
import * as Animatable from 'react-native-animatable';


const mapStateToProps = state => {
  return {
    dishes: state.dishes
  };
};

class Menu extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Menu"
    };
  };

  render() {
    const renderMenuItem = ({ item, index }) => {
      console.log(index);
      return (
        <Tile
          key={index}
          title={item.name}
          caption={item.description}
          featured
          onPress={() => navigate("DishDetail", { dishId: item.id })}
          imageSrc={{ uri: baseUrl + item.image }}
        />
      );
    };

    const { navigate } = this.props.navigation;

    if (this.props.dishes.isLoading) {
      return <Loading />;
    } else if (this.props.dishes.errMss) {
      return (
        <View>
          <Text>{this.props.dishes.errMss}</Text>
        </View>
      );
    } else {
      return (
        <Animatable.View animation='fadeInRightBig' duration={1000}>
          <FlatList
            data={this.props.dishes.dishes}
            renderItem={renderMenuItem}
            keyExtractor={item => item.id.toString()}
          />
        </Animatable.View>
      );
    }
  }
}

export default connect(mapStateToProps)(Menu);
