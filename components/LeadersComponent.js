import React from 'react';
import {FlatList} from 'react-native';
import {ListItem} from 'react-native-elements';

function LeaderList (props){

    const renderLeader = ({item, index}) => {

        return(
            <ListItem
                key = {index}
                title = {item.name}
                subtitle = {item.description}
                leftAvatar = {{ source : require('./images/alberto.png')}}
            />
        );

    };

    return (
        <FlatList
            data = {props.leaders}
            renderItem = {renderLeader}
            keyExtractor = {(item) => item.id.toString() }
        >

        </FlatList>
    );

}

export default LeaderList;