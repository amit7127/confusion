import React, { Component } from 'react';
import { FlatList, View, Text, Alert } from 'react-native';
import { ListItem } from 'react-native-elements';
import { baseUrl } from '../shared/baseUrl';
import { Loading } from './LoadingComponent';
import { connect } from 'react-redux';
import { deleteFavorite } from '../redux/ActionCreator';
import Swipeout from 'react-native-swipeout';
import * as Animatable from 'react-native-animatable';


const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        favorites: state.favorites
    }
}

const mapDispatchToProps = dispatch => ({
    deleteFavorite: dishId => dispatch(deleteFavorite(dishId))
})

class Favorites extends Component {

    static navigationOptions = {
        title: 'My Favorites'
    };

    render() {
        const { navigate } = this.props.navigation;

        const renderFavorite = ({ item, index }) => {

            const rightButton = [
                {
                    text: 'Delete',
                    type: 'delete',
                    onPress: () => {
                        Alert.alert(
                            'Delete Favorite??',
                            'Are you realy want to delete the favorite ' + item.name + ' ??',
                            [
                                {
                                    text: 'Cancle',
                                    onPress: () => console.log('Not deleted' + item.name),
                                    style: 'cancel'
                                },
                                {
                                    text: 'Ok',
                                    onPress: () => { this.props.deleteFavorite(item.id) },
                                }
                            ],
                            { cancelable: false }
                        )
                    }
                }
            ];

            return (

                <Swipeout right={rightButton} autoClose={true}>
                    <Animatable.View animation='fadeInRightBig' duration={1000}>
                        <ListItem
                            key={index}
                            title={item.name}
                            subtitle={item.description}
                            onPress={() => navigate('DishDetail', { dishId: item.id })}
                            leftAvatar={{ source: { uri: baseUrl + item.image } }}
                        />
                    </Animatable.View>
                </Swipeout>
            );
        };
        if (this.props.dishes.isLoading) {
            return (
                <Loading />
            );
        } else if (this.props.dishes.errMess) {
            return (
                <View>
                    <Text>{this.props.dishes.errMess}</Text>
                </View>
            );
        } else {
            return (
                <FlatList
                    data={this.props.dishes.dishes.filter(dish => this.props.favorites.some(el => el === dish.id))}
                    renderItem={renderFavorite}
                    keyExtractor={item => item.id.toString()}
                />
            );
        }

    }

}

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);