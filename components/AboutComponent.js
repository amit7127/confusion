import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Image
} from "react-native";
import { Card, ListItem } from "react-native-elements";
import { connect } from "react-redux";
import { baseUrl } from "../shared/baseUrl";
import { Loading } from "./LoadingComponent";
import * as Animatable from 'react-native-animatable';

const mapStateToProps = state => {
  return { leaders: state.leaders };
};

class About extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "About Us"
    };
  };

  History() {
    return (
      <Card title="Our History">
        <Text style={{ margin: 10 }}>
          Started in 2010, Ristorante con Fusion quickly established itself as a
          culinary icon par excellence in Hong Kong. With its unique brand of
          world fusion cuisine that can be found nowhere else, it enjoys
          patronage from the A-list clientele in Hong Kong. Featuring four of
          the best three-star Michelin chefs in the world, you never know what
          will arrive on your plate the next time you visit us.
          {"\n\n"}
          The restaurant traces its humble beginnings to The Frying Pan, a
          successful chain started by our CEO, Mr. Peter Pan, that featured for
          the first time the world's best cuisines in a pan.
        </Text>
      </Card>
    );
  }

  render() {
    const renderLeader = ({ item, index }) => {
      return (
        <ListItem
          key={index}
          title={item.name}
          subtitle={item.description}
          leftAvatar={{ source: { uri: baseUrl + item.image } }}
        />
      );
    };

    if (this.props.leaders.isLoading) {
      return (
        <ScrollView>
          {this.History()}
          <Card title="Corporate Leadership">
            <Loading />
          </Card>
        </ScrollView>
      );
    } else if (this.props.leaders.errMss) {
      return (
        <ScrollView>
          <Animatable.View animation='fadeInDown' duration={2000} delay={1000}>
            {this.History()}
            <Card title="Corporate Leadership">
              <Text>{this.props.leaders.errMss}</Text>
            </Card>
          </Animatable.View>
        </ScrollView>
      );
    } else {
      return (
        <View>
          <ScrollView>
            <Animatable.View animation='fadeInDown' duration={2000} delay={1000}>
              {this.History()}
              <Card title="Corporate Leadership">
                <FlatList
                  data={this.props.leaders.leaders}
                  renderItem={renderLeader}
                  keyExtractor={item => item.id.toString()}
                />
              </Card>
              <View style={{ height: 15 }} />
            </Animatable.View>
          </ScrollView>
        </View>
      );
    }
  }
}

export default connect(mapStateToProps)(About);
