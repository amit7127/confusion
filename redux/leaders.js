import * as ActionTypes from "./ActionTypes";

export const leaders = (
  state = {
    isLoading: false,
    errMss: null,
    leaders: []
  },
  action
) => {
  switch (action.type) {
    case ActionTypes.ADD_LEADERS:
      return {
        ...state,
        isLoading: false,
        errMss: null,
        leaders: action.payload
      };
    case ActionTypes.LEADERS_LOADING:
      return { ...state, isLoading: true, errMss: null, leaders: [] };
    case ActionTypes.LEADERS_FAILED:
      return {
        ...state,
        isLoading: false,
        errMss: action.payload,
        leaders: []
      };
    default:
      return state;
  }
};
